@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="content ">
    <div class="container-fluid p-b-50 p-t-25">
        <div class="row">
            <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                <div class="row">
                    <div class="col-md-12 m-b-10">
                        <!-- START WIDGET D3 widget_graphTileFlat-->
                        <div id="credits" class="widget-8 card no-border bg-primary no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header  top-left top-right">
                                            <div class="card-title">
                                                <span
                                                    class="font-montserrat fs-11 all-caps text-white font-weight-bold">Account
                                                    Balance
                                                </span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-refresh text-black"
                                                            href="#"><i class="card-icon card-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height ">
                                    <div class="col-xs-height col-top">
                                        <div class="row p-l-20 p-r-20">
                                            <div class="col-sm-6" style="width: 50%;">
                                                <h4 class="p-b-5 m-b-5 text-white text-center">
                                                    $12,500
                                                    <br>
                                                    <small class="fs-10 all-caps">Credits</small>
                                                </h4>
                                            </div>
                                            <div class="col-sm-6" style="width: 50%;">
                                                <h4 class="p-b-5 m-b-5 text-white text-center">
                                                    $14,000
                                                    <br><small class="fs-10 all-caps m-b-5">cad$</small>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                <div class="row">
                    <div class="col-md-12 m-b-10">
                        <!-- START WIDGET D3 widget_graphTileFlat-->
                        <div class="widget-8 card no-border bg-success no-margin widget-loader-bar">
                            <div class="container-xs-height full-height">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header  top-left top-right">
                                            <div class="card-title">
                                                <span
                                                    class="font-montserrat fs-11 all-caps text-white font-weight-bold">Weekly
                                                    Spending <i class="fa fa-chevron-right"></i>
                                                </span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-refresh text-black"
                                                            href="#"><i class="card-icon card-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height ">
                                    <div class="col-xs-height col-top relative">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="p-l-20">
                                                    <h3 class="p-b-5 m-b-5 text-white">$14,000</h3>
                                                    <p class="small m-t-5 p-t-5">
                                                        <a href="#!" class="fs-10 text-white">VIEW
                                                            DETAILS</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                            </div>
                                        </div>
                                        <div class="widget-8-chart line-chart" data-line-color="black"
                                            data-points="true" data-point-color="success" data-stroke-width="2">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                <div class="row">
                    <div class="col-md-12 m-b-10">
                        <!-- START WIDGET D3 widget_graphTileFlat-->
                        <div class="widget-8 card no-border bg-success no-margin widget-loader-bar"
                            style="background: url('{{asset('assets/img/dashboard_tile-envelopes.jpg')}}') no-repeat center;">
                            <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header  top-left top-right">
                                            <div class="card-title">
                                                <span
                                                    class="font-montserrat fs-11 all-caps text-white font-weight-bold">Envelopes
                                                </span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-refresh text-black"
                                                            href="#"><i class="card-icon card-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height ">
                                    <div class="col-xs-height col-top relative">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="p-l-20">
                                                    <!-- <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> -->
                                                    <p class="small m-t-5 p-t-5">
                                                        <a href="#!" class="fs-10 text-white">VIEW
                                                            DETAILS</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xl-3 col-xlg-2 ">
                <div class="row">
                    <div class="col-md-12 m-b-10">
                        <!-- START WIDGET D3 widget_graphTileFlat-->
                        <div class="widget-8 card no-border bg-success no-margin widget-loader-bar"
                            style="background: url('{{asset('assets/img/dashboard_tile-support.jpg')}}') no-repeat center;">
                            <div class="container-xs-height full-height" style="background-color: rgba(0, 0, 0, 0.6)">
                                <div class="row-xs-height">
                                    <div class="col-xs-height col-top">
                                        <div class="card-header  top-left top-right">
                                            <div class="card-title">
                                                <span
                                                    class="font-montserrat fs-11 all-caps text-white font-weight-bold">Support
                                                </span>
                                            </div>
                                            <div class="card-controls">
                                                <ul>
                                                    <li>
                                                        <a data-toggle="refresh" class="card-refresh text-black"
                                                            href="#"><i class="card-icon card-icon-refresh"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-xs-height ">
                                    <div class="col-xs-height col-top relative">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="p-l-20">
                                                    <!-- <h3 class="p-b-5 m-b-5 text-white">$14,000</h3> -->
                                                    <p class="small m-t-5 p-t-5">
                                                        <a href="#!" class="fs-10 text-white">VIEW
                                                            DETAILS</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-5 col-xlg-5 p-b-5">
                <div
                    class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle full-height d-flex flex-column">
                    <div class="card-header  top-right">
                        <div class="card-controls">
                            <ul>
                                <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                            class="card-icon card-icon-refresh"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="padding-25">
                        <div class="pull-left">
                            <h2 class="no-margin">My Receipts</h2>
                            <p class="no-margin">Recent Transactions</p>
                        </div>
                        <h3 class="pull-right semi-bold"><sup>
                                <small class="semi-bold">$</small>
                            </sup> 102,967
                        </h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="auto-overflow widget-11-2-table">
                        <table class="table table-condensed table-hover table-responsive">
                            <thead>
                                <tr class="text-center">
                                    <th class="all-caps">Date</th>
                                    <th class="all-caps">Order ID</th>
                                    <th class="all-caps">Merchant Name</th>
                                    <th class="all-caps">amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                                <tr class="text-center">
                                    <td class="fs-12">05/17/2019</td>
                                    <td class="fs-12"><a href="#!">2113</a></td>
                                    <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                    <td class="fs-12">$222.00</td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                    <div class="padding-25 mt-auto">
                        <p class="small">
                            <a href="#"><span>Go To Transactions Manager</span> <i
                                    class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-xl-3 col-xlg-3">
                <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle ">
                    <div class="card-header  top-right">
                        <div class="card-controls">
                            <ul>
                                <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                            class="card-icon card-icon-refresh"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="padding-25">
                        <div class="pull-left">
                            <h2 class="no-margin">My Envelopes</h2>
                            <!-- <p class="no-margin">Recent Transactions</p> -->
                        </div>
                        <!-- <h3 class="pull-right semi-bold"><sup>
											<small class="semi-bold">$</small>
										</sup> 102,967
									</h3> -->
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-11-2-table">
                        <div id="tableEnvelope_wrapper" class="dataTables_wrapper no-footer">
                            <div>
                                <table class="table table-condensed table-hover dataTable no-footer" id="tableEnvelope"
                                    role="grid">
                                    <thead>
                                        <tr class="text-center" role="row">
                                            <th class="all-caps sorting_asc" tabindex="0" aria-controls="tableEnvelope"
                                                rowspan="1" colspan="1" aria-sort="ascending"
                                                aria-label="Name: activate to sort column descending">Name</th>
                                            <th class="all-caps sorting" tabindex="0" aria-controls="tableEnvelope"
                                                rowspan="1" colspan="1"
                                                aria-label="items: activate to sort column ascending"
                                                style="width: 101px;">items</th>
                                        </tr>
                                    </thead>
                                    <tbody>





                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1"><a href="#!">Name 1</a></td>
                                            <td class="fs-12"><a href="#!">5</a></td>
                                        </tr>
                                        <tr class="text-center even" role="row">
                                            <td class="fs-12 sorting_1"><a href="#!">Name 2</a></td>
                                            <td class="fs-12"><a href="#!">5</a></td>
                                        </tr>
                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1"><a href="#!">Name 3</a></td>
                                            <td class="fs-12"><a href="#!">5</a></td>
                                        </tr>
                                        <tr class="text-center even" role="row">
                                            <td class="fs-12 sorting_1"><a href="#!">Name 4</a></td>
                                            <td class="fs-12"><a href="#!">5</a></td>
                                        </tr>
                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1"><a href="#!">Name 5</a></td>
                                            <td class="fs-12"><a href="#!">5</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="padding-25 mt-auto">
                        <p class="small">
                            <a href="#!"><span>Go To Envelopes Manager</span> <i
                                    class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-xl-4 col-xlg-4">
                <div class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle ">
                    <div class="card-header  top-right">
                        <div class="card-controls">
                            <ul>
                                <li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
                                            class="card-icon card-icon-refresh"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="padding-25">
                        <div class="pull-left">
                            <h2 class="no-margin">My Stores</h2>
                            <!-- <p class="no-margin">Recent Transactions</p> -->
                        </div>
                        <!-- <h3 class="pull-right semi-bold"><sup>
											<small class="semi-bold">$</small>
										</sup> 102,967
									</h3> -->
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-11-2-table">
                        <div id="tableStore_wrapper" class="dataTables_wrapper no-footer">
                            <div>
                                <table class="table table-condensed table-hover table-responsive dataTable no-footer"
                                    id="tableStore" role="grid">
                                    <thead>
                                        <tr class="text-center" role="row">
                                            <th style="width: 20%;" class="all-caps sorting_asc" tabindex="0"
                                                aria-controls="tableStore" rowspan="1" colspan="1" aria-sort="ascending"
                                                aria-label="rank: activate to sort column descending">rank</th>
                                            <th style="width: 70px;" class="all-caps sorting" tabindex="0"
                                                aria-controls="tableStore" rowspan="1" colspan="1"
                                                aria-label="merchant: activate to sort column ascending">merchant</th>
                                            <th style="width: 32px;" class="all-caps sorting" tabindex="0"
                                                aria-controls="tableStore" rowspan="1" colspan="1"
                                                aria-label="qty: activate to sort column ascending">qty</th>
                                            <th style="width: 68px;" class="all-caps sorting" tabindex="0"
                                                aria-controls="tableStore" rowspan="1" colspan="1"
                                                aria-label="spending: activate to sort column ascending">spending</th>
                                        </tr>
                                    </thead>
                                    <tbody>





                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1">1</td>
                                            <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                            <td class="fs-12">12</td>
                                            <td class="fs-12">$50.00</td>
                                        </tr>
                                        <tr class="text-center even" role="row">
                                            <td class="fs-12 sorting_1">2</td>
                                            <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                            <td class="fs-12">12</td>
                                            <td class="fs-12">$50.00</td>
                                        </tr>
                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1">3</td>
                                            <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                            <td class="fs-12">12</td>
                                            <td class="fs-12">$50.00</td>
                                        </tr>
                                        <tr class="text-center even" role="row">
                                            <td class="fs-12 sorting_1">4</td>
                                            <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                            <td class="fs-12">12</td>
                                            <td class="fs-12">$50.00</td>
                                        </tr>
                                        <tr class="text-center odd" role="row">
                                            <td class="fs-12 sorting_1">5</td>
                                            <td class="fs-12"><a href="#!">Merchant 1</a></td>
                                            <td class="fs-12">12</td>
                                            <td class="fs-12">$50.00.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="padding-25 mt-auto">
                        <p class="small">
                            <a href="#!"><span>Go To Stores Manager</span> <i
                                    class="fa fs-12 fa-arrow-circle-o-right text-success m-l-10"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('scripts')

<script>
    $(document).ready(function (e) {
        $("#tableEnvelope").dataTable({
            "sDom": "<t><'row'<p i>>",
            "paging": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        })

        $("#tableStore").dataTable({
            "sDom": "<t><'row'<p i>>",
            "order": [],
            "paging": false,
            "info": false,
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        })
   
    })
</script>
<!-- END PAGE LEVEL JS -->
@endsection
