@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">

            <div class="card" id="RecipeTableCard">
                <div class="card-header row">
                    <!-- <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong> -->
                    <div class="col-md-8">
                        <strong>Select Recipe</strong>
                    </div>
                    <div class="col-md-4 form-group">
                        <label for="recipesFrom">Recipes From</label>
                        <div class="form-check form-check-inline float-right">
                            <input class="form-check-input" type="checkbox" id="scrapedOrNot" checked="true">
                            <label class="form-check-label" for="scrapedOrNot">
                                <small id="scrapedOrNotText" class="form-text text-muted">
                                    not Scraped
                                </small>
                            </label>
                        </div>
                        <select class="form-control" id="recipesFrom" aria-describedby="Recipes Senders">
                        </select>
                    </div>
                </div><!--card-header-->
                <div class="card-body">
                    <table class="table table-bordered data-table table-striped" id="RecipeData">
                        <thead>
                            <tr class="table-active">
                                <th>Recipe From</th>
                                <th>Vendor</th>
                                <th>Received</th>
                                <th>Downloaded</th>
                                <th>Scraped</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!--card-body-->
            </div><!--card-->

            <div class="card" id="RecipeConfigCard">
                <div class="card-header">
                    <!-- <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong> -->
                    <div class="col-md-12">
                        <strong>Recipe Config</strong>
                    </div>
                </div><!--card-header-->
                <div class="card-body row">
                    <div class="col-md-6" >
                    <pre id="scrapedData" class="form-control col-md-12" style="height:100%;"></pre>
                    </div>
                    <div class="col-md-6 row">
                        <div class="form-group col-md-8">
                            <lable class="form-label" for="selector">Selector:</lable>
                            <div class="form-inline">
                                <input type="text" class="form-control config-setting col-md-12" name="selector" id="selector" value="html">
                            </div>
                        </div>
                        <div class="form-group col-md-4" id="positionDiv">
                            <lable class="form-label" for="position">Position:</lable>
                            <div class="form-inline">
                                <input type="number" class="form-control config-setting col-md-12" name="position" id="position">
                            </div>
                        </div>
                        <div class="form-group col-md-8"  id="replaceDiv">
                            <lable class="form-label" for="replace">Replace Text:</lable>
                            <div class="form-inline">
                                <input type="text" class="form-control config-setting col-md-12" name="replace" id="replace">
                            </div>
                        </div>
                        <!-- <div class="form-group col-md-4">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="ignoreTags" checked="False">
                            <label class="form-check-label" for="ignoreTags">
                                <small id="ignoreTagsText" class="form-text text-muted">
                                Ignore HTML Tags
                                </small>
                            </label>
                        </div>
                        </div> -->
                        <div class="form-group col-md-12">
                            <lable class="form-label" for="configuaration">Configuaration :</lable>
                            <div class="form-inline">
                                <input type="text" class="form-control col-md-12" name="configuaration" id="configuaration" placeholder="html selector {position}(replace)" readonly>
                                <input type="text" class="form-control config-setting col-md-12" name="configFlag" id="configFlag" value="" hidden>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <button class="form-control btn btn-warning float-right col-md-3" id="clearConfigValue">Clear</button>
                            <button class="form-control btn btn-success float-right col-md-3" id="setConfigValue">Set</button>
                        </div>
                    </div>
                </div><!--card-body-->
            </div><!--card-->

            <div class="card" id="RecipeHtmlCard">
                <div class="card-header">
                    <!-- <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong> -->
                    <strong>Recipe</strong>
                    <button class="btn btn-info float-right" id="newConfig">New Configuration</button>
                    <input type="text" name="recipe_id" id="recipe_id" hidden>
                    <input type="text" name="recipe_config_id" id="recipe_config_id" hidden>
                </div><!--card-header-->
                <div class="card-body row">
                    <div class="col-md-6" id="recipe_html" style="overflow: scroll;height: 1600px;"></div>
                    <div class="col-md-6"  id="recipe_text">
                        <div class="form-group">
                            <lable class="form-label" for="Vendor">Vendor :</lable>
                            <div class="form-inline">
                                <select class="form-control col-sm-10" name="vendor" id="vendor"></select>
                                <button class="btn btn-success col-sm-2" data-toggle="modal" data-target="#addVendorModal">Add</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_no">Recipe No. Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_no_flag" id="recipe_no_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_trans_no_flag">Transaction No. Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_trans_no_flag" id="recipe_trans_no_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_trans_date_flag">Transaction Date Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_trans_date_flag" id="recipe_trans_date_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_trans_time_flag">Transaction Time Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_trans_time_flag" id="recipe_trans_time_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_trans_time_flag">Transaction Time Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_trans_time_flag" id="recipe_trans_time_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_employee_no_flag">Employee No. Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_employee_no_flag" id="recipe_employee_no_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_employee_name_flag">Employee Name Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_employee_name_flag" id="recipe_employee_name_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_subtotal_amount_flag">Subtotal Amount Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_subtotal_amount_flag" id="recipe_subtotal_amount_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_total_tax_flag">TAX/HST Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_total_tax_flag" id="recipe_total_tax_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_total_tip_flag">Tip Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_total_tip_flag" id="recipe_total_tip_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_delivery_fee_flag">Delivery Fee Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_delivery_fee_flag" id="recipe_delivery_fee_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_total_amount_flag">Total Amount Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_total_amount_flag" id="recipe_total_amount_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_note_flag">Note Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_note_flag" id="recipe_note_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_terminal_flag">Terminal Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_terminal_flag" id="recipe_terminal_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_list_flag">Product List Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_list_flag" id="recipe_product_list_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_code_flag">Product Code Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_code_flag" id="recipe_product_code_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_name_flag">Product Name Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_name_flag" id="recipe_product_name_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_unit_price_flag">Product Unit Price Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_unit_price_flag" id="recipe_product_unit_price_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_quantity_flag">Product Quantity Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_quantity_flag" id="recipe_product_quantity_flag" readonly>
                        </div>
                        <div class="form-group">
                            <lable class="form-label" for="recipe_product_amount_flag">Product Amount Flag : </lable>
                            <input class="form-control clickable" type="text" name="recipe_product_amount_flag" id="recipe_product_amount_flag" readonly>
                        </div>
                        <div class="form-group form-inline">
                            <button class="btn btn-warning" id="clearConfig">Clear</button>
                            <button class="btn btn-success" id="addConfig">Add Config</button>
                            <button class="btn btn-info" id="editConfig">Modify Config</button>
                            <button class="btn btn-danger" id="deleteConfig">Remove Config</button>
                        </div>
                    </div>
                </div><!--card-body-->
            </div><!--card-->

        </div><!--col-->
    </div><!--row-->
    <div class="modal fade" id="addVendorModal" tabindex="-1" role="dialog" aria-labelledby="addVendorLable" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addVendorLable">Add Vendor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body "><form action="" class="addVendor">
                    <div class="form-group">
                        <label for="vendor_name" class="form-label">Name :</label>
                        <input type="text" name="vendor_name" id="vendor_name" class="form-control ">
                    </div>
                    <div class="form-group">
                        <label for="vendor_email" class="form-label">Email :</label>
                        <input type="email" name="vendor_email" id="vendor_email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="vendor_address" class="form-label">Address :</label>
                        <input type="text" name="vendor_address" id="vendor_address" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="vendor_store" class="form-label">Store :</label>
                        <input type="text" name="vendor_store" id="vendor_store" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="vendor_tax" class="form-label">TAX/HST :</label>
                        <input type="text" name="vendor_tax" id="vendor_tax" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="vendor_contact_no" class="form-label">Contact No. :</label>
                        <input type="text" name="vendor_contact_no" id="vendor_contact_no" class="form-control">
                    </div>
                </div></form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveNewVendor">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }

    });
    $( document ).ready(function() {
        $("#RecipeHtmlCard").hide();
        $("#RecipeConfigCard").hide();
        getScrapeSel(); 
    });
    $("#scrapedOrNot").change(function() {
        getScrapeSel();
    });       
    $('#RecipeData tbody').on('click', 'tr', function () {
        $("#RecipeHtmlCard").show();
        $("#RecipeConfigCard").show();
        $(".config-setting").val(null);
        $("#scrapedData").html(null);
        $("#recipe_id").val(null);
        recipeSelected = $('#RecipeData').DataTable().row(this).data();
        displayRecipe(recipeSelected.recipe_id);
        getVendorList(recipeSelected);
        $("#recipe_id").val(recipeSelected.recipe_id);
        fillRecipeConfig(recipeSelected.vendor_id);
        fillScrapedDataByVendor(recipeSelected.recipe_id,recipeSelected.vendor_id);
        $("#addConfig").hide();
        $("#editConfig").show();
    } );
    $(".config-setting").on("change keyup",function() {
        configVal=$("#selector").val().concat("{",$("#position").val(),"}(",$("#replace").val(),")");
        $("#configuaration").val(configVal);

        configValPass=$("#configFlag").val().concat(":",configVal);
        fillScrapedDataByConfig($("#recipe_id").val(),$("#configFlag").val(),configVal);

    });
    $("#recipesFrom").change(function() {
        getRecipeTable();
    });
    $("#newConfig").click(function(){
        $(".clickable").val(null);
        getVendorList();
        $("#addConfig").show();
        $("#editConfig").hide();
        $("#scrapedData").html(null);
    });
    $("#vendor").change(function(){
        fillRecipeConfig($('#vendor').val());
        fillScrapedDataByVendor($('#recipe_id').val(),$('#vendor').val());
        $("#addConfig").hide();
        $("#editConfig").show();
    });
    $("#saveNewVendor").click(function(){
        $.ajax({
            type:'GET',
            url:'ajax/ajaxVendor',
            data:{'function_name':'saveNewVendor','vendor_name':$("#vendor_name").val(),'vendor_email':$("#vendor_email").val(),'vendor_contact_no':$("#vendor_contact_no").val(),'vendor_address':$("#vendor_address").val(),'vendor_store':$("#vendor_store").val(),'vendor_tax':$("#vendor_tax").val()},
            success:function(data){
                if(data && data.success){
                    recipeSelected['vendor_id']=data.last_insert_id;
                    recipeSelected['name']=$("#vendor_name").val();
                    getVendorList(recipeSelected);
                    $("#addVendorModal").modal('toggle');
                    $(".clickable").val(null);
                }
            }
        });
      
    });
    $(".clickable").click(function(){
        $("#selector").focus();
        value = $(this).val().toString();
        $("#configFlag").val($(this).attr("name"));
        $("#selector").val((value)?value.slice(0, value.indexOf('{')):'html' );
        $("#position").val( value.slice( value.indexOf("{")+1, value.indexOf('}') ) );
        $("#positionDiv").show();
        $("#replaceDiv").show();
        $("#replace").val( value.slice( value.indexOf('(')+1, value.indexOf(')') ) );
        $("#configuaration").val(value);
        fillScrapedDataByConfig($("#recipe_id").val(),$(this).attr("name"),(value)?value:'html' );
    });
    $("#recipe_product_list_flag").click(function(){
        $("#position").val(null);
        $("#positionDiv").hide();
        $("#replace").val(null);
        $("#replaceDiv").hide();
    });
    $("#clearConfigValue").click(function(){
        $(".config-setting").val(null);
    });
    $("#setConfigValue").click(function(){
        if(!$("#position").val() && $("#configFlag").val()!="recipe_product_list_flag"){
            alert("Set Position Value");
        }else{
            inputName="#".concat($("#configFlag").val());
            $(inputName).val($("#configuaration").val());
            $(inputName).focus();
        }
    });
    $("#addConfig").click(function(){

    });
    $("#deleteConfig").click(function(){
        
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceiptConfig',
            data:{'function_name':'deleteRecipeConfig','recipe_config_id':$("#recipe_config_id").val()},
            success:function(data){
                if(data.status="success"){
                    alert("Recipe Configuration Deleted");
                }
            }
        });
    });
    $("#clearConfig").click(function(){
        $(".clickable").val(null);
    });
    function getScrapeSel(){
        scraped=$('#scrapedOrNot').is(':checked')?1:0;
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceipt',
            data:{'function_name':'getScrapeSel','scraped':scraped},
            success:function(data){
                $('#recipesFrom').empty()
                .append($("<option></option>")
                .attr("value","all")
                .text('Select Recipe Sender'));
                data.forEach(function(element){
                    $('#recipesFrom').append($("<option></option>")
                    .attr("value",element['recipe_mail'])
                    .text((!element['vendor_name'])?element['recipe_mail']:element['vendor_name']));
                });
            }
        });
        getRecipeTable();
    }
    function getRecipeTable(){
        recipe_from=$('#recipesFrom').val();
        scraped=$('#scrapedOrNot').is(':checked')?1:0;
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceipt',
            data:{'function_name':'getRecipeTable','recipe_from':recipe_from,'scraped':scraped},
            success:function(data){
                $('#RecipeData').DataTable({
                    "destroy": true,
                    "fixedHeader": true,
                    "initComplete": function( settings, json ) {
                        //$('#loading-screen').css('display', 'none');
                        //$('#ajax-content').css('filter', 'unset');
                    },
                    "data": data,
                    "paging": false,
                    "searching":false,
                    "columns": [
                        { "data": "mail_from" },
                        { "data": "name" },
                        { "data": "recieved_at" },
                        { "data": "downloaded_at" },
                        { "data": "scrape_at" },
                    ],
                });
            }

        });
    }
    function displayRecipe(recipeSelected){
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceipt',
            data:{'function_name':'getRecipeHtml','recipe_id':recipeSelected},
            success:function(data){
                $("#recipe_html").html(data.recipeHtml);
            }
        });
    }
    function getVendorList(recipeSelected){
        $.ajax({
            type:'GET',
            url:'ajax/ajaxVendor',
            data:{'function_name':'getVendors','vendor_id':0},
            success:function(data){
                $('#vendor').empty();
                if(recipeSelected && recipeSelected.vendor_id){
                    $('#vendor').append($("<option></option>")
                    //.attr("value",data.id)
                    .attr("value",recipeSelected.vendor_id)
                    .attr("hidden",true)
                    .text(recipeSelected.name));
                    //.text(data.name));
                }else{
                    $('#vendor').append($("<option></option>")
                    .attr("hidden",true)
                    .text('Select Vendor'));
                }
                data.forEach(function(element){
                    $('#vendor').append($("<option></option>")
                    .attr("value",element['id'])
                    .text(element['name']));
                });
            }
        });
    }
    function getRecipeConfig(vendor_id){
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceiptConfig',
            data:{'function_name':'getRecipeConfig','vendor_id':vendor_id},
            success:function(data){
            }
        });
    }
    function fillRecipeConfig(vendor_id){
        $(".clickable").val(null);
        if(!vendor_id){
            return ; 
        }
        
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceiptConfig',
            data:{'function_name':'getRecipeConfig','vendor_id':vendor_id},
            success:function(data){
                for (var key in data) {
                    if (!data.hasOwnProperty(key)) continue;
                }
                var obj = data[key];
                $("#recipe_config_id").val(obj.id);
                for (var prop in obj) {
                    if (!obj.hasOwnProperty(prop)) continue;
                    if (prop.search('recipe')!=0) continue;
                    $("#"+prop).val(obj[prop]);
                }

            }
        });
    }
    function fillScrapedDataByConfig(recipeId,scrapeFlag,selector){
        $("#scrapedData").html(null);
        if(scrapeFlag.search("_product_")!=-1 && scrapeFlag.search("_list_")==-1){
            product_list_flag=$("#recipe_product_list_flag").val().replace("{}()","");
            product_list_flag=product_list_flag.substring(0, product_list_flag.lastIndexOf(" "));
            selector=product_list_flag.concat("---",selector);
        }
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceipt',
            data:{'function_name':'RecipeScrapeByConfig','recipe_id':recipeId,'scrape_flag':scrapeFlag,'selector':selector},
            success:function(data){
                if(data.status="success"){
                    Object.keys(data.data.recipe_header).forEach(function(key) {
                        if(data.data.recipe_header[key]){
                            $("#scrapedData").append("<b>"+key+": </b>"+data.data.recipe_header[key]+"</br>");
                        }
                    });
                    if(data.data.recipe_data){
                        Object.keys(data.data.recipe_data).forEach(function(key1) {
                            $("#scrapedData").append("<b>"+[key1]+": </b>"+data.data.recipe_data[key1]+", ");
                        });
                    }
                }
            }
        });
    }
    function fillScrapedDataByVendor(recipeId,vendorId){
        //recipeConfig is a array contains scrape configuration
        $("#scrapedData").html(null);
        $.ajax({
            type:'GET',
            url:'ajax/ajaxReceipt',
            data:{'function_name':'RecipeScrapeByVendor','recipe_id':recipeId,'vendor_id':vendorId},
            success:function(data){
                if(data.status="success"){
                    Object.keys(data.data.recipe_header).forEach(function(key) {
                        if(data.data.recipe_header[key]){
                            $("#scrapedData").append("<b>"+key+": </b>"+data.data.recipe_header[key]+"</br>");
                        }
                    });
                    if(data.data.recipe_data){
                        $("#scrapedData").append("<center><b>Product List...</b></center></br>");
                        Object.keys(data.data.recipe_data).forEach(function(key1) {
                            Object.keys(data.data.recipe_data[key1]).forEach(function(key2) {
                                $("#scrapedData").append("<b>"+[key2]+": </b>"+data.data.recipe_data[key1][key2]+", ");
                            });
                            $("#scrapedData").append("<br>");
                        });
                    }
                }
            }
        });
    }
@endsection
