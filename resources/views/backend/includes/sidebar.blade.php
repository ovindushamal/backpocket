<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->

    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
    <img src="{{ asset('img/backend/brand/logo.svg') }}" alt="logo" data-src="{{ asset('img/backend/brand/logo.svg') }}"
            data-src-retina="{{ asset('img/backend/brand/logo.svg') }}" width="78" height="22"> 
					
        <div class="sidebar-header-controls">
            <button type="button"
                class="btn btn-link d-lg-inline-block d-xlg-inline-block d-md-inline-block d-sm-none d-none"
                data-toggle-pin="sidebar"><i class="fa fs-12"></i>
            </button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
       
        <ul class="menu-items">
        
          <li class="m-t-30 ">
            <a href="{{ route('admin.dashboard') }}"><span class="title">@lang('menus.backend.sidebar.dashboard')</span></a>
            <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>
    
          @if ($logged_in_user->isAdmin())
          <li>
            <a href="javascript:;"><span class="title">@lang('menus.backend.access.title')
            @if ($pending_approval > 0)
                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
            </span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-signals"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ route('admin.auth.user.index') }}">
                @lang('labels.backend.access.users.management')

@if ($pending_approval > 0)
<span class="badge badge-danger">{{ $pending_approval }}</span>
@endif
</a>
                <span class="icon-thumbnail">m</span>
              </li>
              <li class="">
                <a href="{{ route('admin.auth.role.index') }}">
                @lang('labels.backend.access.roles.management')</a>
                <span class="icon-thumbnail">L</span>
              </li>
            
            </ul>
          </li>

          <li>
            <a href="javascript:;"><span class="title">@lang('menus.backend.log-viewer.main')</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i class="pg-note"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="{{ route('log-viewer::dashboard') }}"> @lang('menus.backend.log-viewer.dashboard')</a>
                <span class="icon-thumbnail">D</span>
              </li>
              <li class="">
                <a href="{{ route('log-viewer::logs.list') }}">@lang('menus.backend.log-viewer.logs')</a>
                <span class="icon-thumbnail">L</span>
              </li>
         
           
            </ul>
          </li>
      
          @endif
        </ul>
       

        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>
