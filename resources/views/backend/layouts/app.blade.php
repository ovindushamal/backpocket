<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Laravel Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    <link href="{{asset('assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{asset('assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{asset('assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{asset('assets/plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{asset('assets/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css"
		media="screen">
	<link href="{{ asset('assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet" type="text/css"
		media="screen" />
	<link href="{{ asset('assets/plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css" media="screen" />
	<link href="{{ asset('pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
	
    @yield('meta')
    
    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/backend.css')) }}
    
    @stack('after-styles')

    
</head>

<body class="fixed-header ">
	<!-- BEGIN SIDEBPANEL-->
    @include('backend.includes.sidebar')
	<!-- END SIDEBAR -->
	<!-- END SIDEBPANEL-->
	<!-- START PAGE-CONTAINER -->
	<div class="page-container ">
		<!-- START HEADER -->
        @include('backend.includes.header')
		<!-- END HEADER -->
		<!-- START PAGE CONTENT WRAPPER -->
		<div class="page-content-wrapper ">
			<!-- START PAGE CONTENT -->
            @include('includes.partials.messages')
                    @yield('content')
			<!-- END PAGE CONTENT -->
			<!-- START COPYRIGHT -->
			<!-- START CONTAINER FLUID -->
			<!-- START CONTAINER FLUID -->
			<div class=" container-fluid  container-fixed-lg footer">
				<div class="copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						&copy;2019 Backpocket Inc.</span><span class="hint-text"> All Rights Reserved</span>
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END COPYRIGHT -->
		</div>
		<!-- END PAGE CONTENT WRAPPER -->
	</div>
	<!-- END PAGE CONTAINER -->

	<!-- BEGIN VENDOR JS -->
	<script src="{{ URL::to('assets/plugins/nvd3/src/tooltip.js') }}" type="text/javascript"></script>
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/backend.js')) !!}
    @stack('after-scripts')
   
   	<script src="{{ URL::to('assets/plugins/pace/pace.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/modernizr.custom.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/popper/umd/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-actual/jquery.actual.min.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

	
    <script type="text/javascript" src="{{ URL::to('assets/plugins/select2/js/select2.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::to('assets/plugins/classie/classie.js') }} "></script>
	<script src="{{ URL::to('assets/plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/lib/d3.v3.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/nv.d3.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/src/utils.js') }}" type="text/javascript"></script>

	<script src="{{ URL::to('assets/plugins/nvd3/src/interactiveLayer.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/src/models/axis.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/src/models/line.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/nvd3/src/models/lineWithFocusChart.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/mapplic/js/hammer.min.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/mapplic/js/jquery.mousewheel.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/mapplic/js/mapplic.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/rickshaw/rickshaw.min.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/jquery-metrojs/MetroJs.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/skycons/skycons.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
	<script src="{{ URL::to('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"
		type="text/javascript"></script>

	<script type="text/javascript" src="{{ URL::to('assets/plugins/datatables-responsive/js/datatables.responsive.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

   <!-- BEGIN CORE TEMPLATE JS -->
	<!-- BEGIN CORE TEMPLATE JS -->
	 <script src="{{ URL::to('assets/js/pages.js') }}"></script>
	<!-- END CORE TEMPLATE JS -->
	<!-- BEGIN PAGE LEVEL JS -->
	<script src="{{ URL::to('assets/js/scripts.js') }}" type="text/javascript"></script>
	<!-- END PAGE LEVEL JS -->
	<script type="text/javascript">
        @yield('scripts')
    </script>
	
	<!-- END PAGE LEVEL JS -->
</body>

</html>