<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('recipe_id')->nullable();
            $table->foreign('recipe_id')->references('id')->on('recipes');
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->string('recipe_no')->nullable();
            $table->string('trans_no')->nullable();
            $table->string('trans_date')->nullable();
            $table->string('trans_time')->nullable();
            $table->string('employee_no')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('subtotal_amount')->nullable();
            $table->string('total_tax')->nullable();
            $table->string('total_tip')->nullable();
            $table->string('delivery_fee')->nullable();
            $table->string('total_amount')->nullable();
            $table->text('note')->nullable();
            $table->text('terminal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_headers');
    }
}
