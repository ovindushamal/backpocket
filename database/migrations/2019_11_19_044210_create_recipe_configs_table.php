<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->string('vendor_name_flag')->nullable();
            $table->string('vendor_address_flag')->nullable();
            $table->string('vendor_email_flag')->nullable();
            $table->string('vendor_contact_no_flag')->nullable();
            $table->string('recipe_no_flag')->nullable();
            $table->string('recipe_trans_no_flag')->nullable();
            $table->string('recipe_trans_date_flag')->nullable();
            $table->string('recipe_trans_time_flag')->nullable();
            $table->string('recipe_terminal_flag')->nullable();
            $table->string('recipe_employee_no_flag')->nullable();
            $table->string('recipe_employee_name_flag')->nullable();
            $table->string('recipe_subtotal_amount_flag')->nullable();
            $table->string('recipe_total_tax_flag')->nullable();
            $table->string('recipe_total_tip_flag')->nullable();
            $table->string('recipe_delivery_fee_flag')->nullable();
            $table->string('recipe_total_amount_flag')->nullable();
            $table->string('recipe_note_flag')->nullable();
            $table->string('recipe_product_code_flag')->nullable();
            $table->string('recipe_product_name_flag')->nullable();
            $table->string('recipe_product_unit_price_flag')->nullable();
            $table->string('recipe_product_quantity_flag')->nullable();
            $table->string('recipe_product_amount_flag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_configs');
    }
}
