<?php

use Illuminate\Database\Seeder;
use App\Models\Common\RecipeData;

class RecipeDataTableSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        //Create Test Vendor
        RecipeData::create([
            'id' => 1,
            'name' => 'Test Vendor',
            'address' => 'Test Address',
            'store' => 'Test Store',
            'tax' => '0.15',
            'contact_no' => '011-025-654-7789',
            'email' => 'Test@test.com',
            'active' => true,
        ]);

        $this->enableForeignKeys();
    }
}
