<?php

use Illuminate\Database\Seeder;
use App\Models\Common\Product;

class ProductTableSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        //Create Test Vendor
        Product::create([
            'id' => 1,
            'parent_id' => 'Test Vendor',
            'address' => 'Test Address',
            'store' => 'Test Store',
            'tax' => '0.15',
            'contact_no' => '011-025-654-7789',
            'email' => 'Test@test.com',
            'active' => true,
        ]);

        $this->enableForeignKeys();
    }
}
