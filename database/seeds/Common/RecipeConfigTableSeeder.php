<?php

use Illuminate\Database\Seeder;
use App\Models\Common\RecipeConfig;

class RecipeConfigTableSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        //Create Test Vendor
        RecipeConfig::create([
            'id' => 1,
            'name' => 'Awesome Sauce Inc.',
            'address' => 'Test Address',
            'store' => 'Test Store',
            'tax' => '0.15',
            'contact_no' => '011-025-654-7789',
            'email' => 'ovindu@backpocket.ca',
            'active' => true,
        ]);

        $this->enableForeignKeys();
    }
}
