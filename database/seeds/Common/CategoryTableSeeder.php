<?php

use Illuminate\Database\Seeder;
use App\Models\Common\Category;

class CategoryTableSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        //Create Test Category
        Category::create([
            'id' => 1,
            'code' => 'Test001',
            'name' => 'Test Category',
            'description' => 'Test Category Description',
            'active' => true,
        ]);
    }
}
