<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;

class TestDataTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->disableForeignKeys();

        // Reset cached roles and permissions
        resolve(PermissionRegistrar::class)->forgetCachedPermissions();

        // $this->truncateMultiple([
        //     'users',
        //     'password_histories',
        //     'password_resets',
        //     'social_accounts',
        // ]);

        // $this->call(CategoryTableSeeder::class);
        // $this->call(ProductTableSeeder::class);
        $this->call(VendorTableSeeder::class);
        // $this->call(RecipeConfigTableSeeder::class);
        // $this->call(RecipeTableSeeder::class);
        // $this->call(RecipeHeaderTableSeeder::class);
        // $this->call(RecipeDataTableSeeder::class);

        $this->enableForeignKeys();
    }
}
