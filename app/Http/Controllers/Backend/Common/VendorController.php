<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Common\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->stdDateFormat='Y-m-d';
        $this->stdTimeFormat='H:i:s';
        $this->stdDateTimeFormat='Y-m-d H:i:s';
        
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('backend.common.vendor');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Common\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Common\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Common\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Common\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        //
    }

    public function ajaxVendor(){
        switch($_REQUEST['function_name']){
            case 'getVendors': return $this->getVendors($_REQUEST['vendor_id']);
                                break;
            case 'saveNewVendor': return $this->saveNewVendor($_REQUEST);
                                break;
            default:
                return False;
        }
    }

    public static function getVendors($vendor_id){
        $query=DB::table('vendors');
        if($vendor_id!=0){
            $query=$query->where('id','=',$vendor_id);
        }
        return $query->get();
    }
    public function saveNewVendor($data){
        $vendor=new Vendor();
        $vendor->name=$data['vendor_name'];
        $vendor->email=$data['vendor_email'];
        $vendor->store=$data['vendor_store'];
        $vendor->tax=$data['vendor_tax'];
        $vendor->contact_no=$data['vendor_contact_no'];
        $vendor->address=$data['vendor_address'];
        $vendor->active=True;
        if($vendor->save()){
            return response()->json(array('success' => true, 'last_insert_id' => $vendor->id), 200);
        }
        return False;
    }
}
