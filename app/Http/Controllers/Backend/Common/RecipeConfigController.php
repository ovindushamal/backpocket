<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Common\RecipeConfig;
use Illuminate\Http\Request;

class RecipeConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Common\RecipeConfig  $recipeConfig
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeConfig $recipeConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Common\RecipeConfig  $recipeConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeConfig $recipeConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Common\RecipeConfig  $recipeConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeConfig $recipeConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Common\RecipeConfig  $recipeConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeConfig $recipeConfig)
    {
        //
    }
    public function ajaxReceiptConfig(){
        switch($_REQUEST['function_name']){
            case 'getRecipeConfig': 
                return $this->getRecipeConfig($_REQUEST['vendor_id']);
                break;
            case 'deleteRecipeConfig': 
                $result=$this->deleteRecipeConfig($_REQUEST['recipe_config_id']);
                return ["status"=>($result)?"success":"failed","data"=>$result];
                break;
            case 'deleteRecipeConfig': 
                $result=$this->deleteRecipeConfig($_REQUEST['recipe_config_id']);
                return ["status"=>($result)?"success":"failed","data"=>$result];
                break;
            default:
                return False;
        }
    }
    public static function getRecipeConfig($vendor_id){
        $query=DB::table('recipe_configs');
        if($vendor_id!=0){
            $query=$query->where('vendor_id','=',$vendor_id);
        }
        return $query->get();
    }
}
