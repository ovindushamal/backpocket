<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\RecipeData;
use Illuminate\Http\Request;

class RecipeDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Common\RecipeData  $recipeData
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeData $recipeData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Common\RecipeData  $recipeData
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeData $recipeData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Common\RecipeData  $recipeData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeData $recipeData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Common\RecipeData  $recipeData
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeData $recipeData)
    {
        //
    }
}
