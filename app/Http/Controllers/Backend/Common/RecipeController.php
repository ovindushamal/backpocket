<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\Recipe;
use App\Models\Common\RecipeHeader;
use App\Models\Common\RecipeData;
use App\Models\Common\Product;
use App\Http\Controllers\Backend\Common\VendorController;
use App\Http\Controllers\Backend\Common\RecipeConfigController;
use Spatie\Html\Elements\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use ZBateson\MailMimeParser\MailMimeParser;
use KubAT\PhpSimple\HtmlDomParser;
use App\Console\Commands\ScrapeRecipes;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->folderPath=storage_path('emails');
        $this->parser = new MailMimeParser();
        $this->recipeHeader=new RecipeHeader();
        $this->recipeData=new RecipeData();
        $this->explodeChar=[' ',',','$',' × '];//
        $this->stdDateFormat='Y-m-d';
        $this->stdTimeFormat='H:i:s';
        $this->stdDateTimeFormat='Y-m-d H:i:s';
        $this->dateFormats=['H:i:s','d/m/Y, H:i','D, d M Y H:i:s O','M d, Y'];
        $this->navigator=['recipe_no'=>'recipe_no','recipe_trans_no'=>'trans_no','recipe_trans_date'=>'trans_date','recipe_trans_time'=>'trans_time','recipe_terminal'=>'terminal','recipe_employee_no'=>'employee_no','recipe_employee_name'=>'employee_name','recipe_subtotal_amount'=>'subtotal_amount','recipe_total_tax'=>'total_tax','recipe_total_tip'=>'total_tip','recipe_delivery_fee'=>'delivery_fee','recipe_total_amount'=>'total_amount','recipe_note'=>'note'];
        $this->strip_tags=1;
    }   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        
        
        return view('backend.common.recipe');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Common\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Common\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Common\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Common\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        //
    }

    public function ajaxReceipt(){
        $function_name=$_REQUEST['function_name'];
        switch($function_name){
            case 'getRecipeHtml': 
                return $this->getRecipeHtml($_REQUEST['recipe_id']);
                break;
            case 'getRecipeTable':
                return $this->getRecipeTable($_REQUEST['recipe_from'],$_REQUEST['scraped']);
                break;
            case 'getScrapeSel' : 
                return $this->getScrapeSel($_REQUEST['scraped']);
                break;
            case 'RecipeScrapeByVendor':
                $result=$this->getRecipeData($_REQUEST['recipe_id'],RecipeConfigController::getRecipeConfig($_REQUEST['vendor_id']));
                return ["status"=>($result)?"success":"failed","data"=>$result];
                break;
            case 'RecipeScrapeByConfig' : 
                $this->strip_tags=0;
                $reipeConfig=[[$_REQUEST['scrape_flag']=>$_REQUEST['selector']]];
                $result = $this->getRecipeData($_REQUEST['recipe_id'],$reipeConfig);
                return ["status"=>($result)?"success":"failed","data"=>$result]; 
                break;
            default: return 'failed';
        }
    }
    public function getScrapeSel($scraped){
        $query = DB::table('recipes')
                    ->select('recipes.mail_from as recipe_mail','vendors.name as vendor_name')->distinct()
                    ->leftJoin('vendors','recipes.mail_from','=','vendors.email');
        if($scraped!=0)
            $query =$query->whereNull('recipes.scrape_at');
        $query =$query->OrderBy('vendors.name','asc');
        $recipes = $query->get();            
        return $recipes;
    }
    public function getRecipeTable($recipe_from,$scraped){
        $recipe_from=(!$recipe_from)?'all':$recipe_from;
        $query = DB::table('recipes')
                    ->select('*','recipes.id as recipe_id','vendors.id as vendor_id')
                    ->leftJoin('vendors','recipes.mail_from','=','vendors.email');
        if($scraped!=0)
            $query =$query->whereNull('recipes.scrape_at');
        if($recipe_from!='all')
            $query =$query->where('recipes.mail_from','=',$recipe_from);
        $query =$query->OrderBy('vendors.name','asc');
        $recipes = $query->get();            
        return $recipes;
    }
    //function from Command.ScrapeRecipe (scrape:recipe)
    public function getRecipeHtml($recipeId){
        $path=$this->folderPath.'\\'.$recipeId.'.eml';

        if(!file_exists($path)) {
            echo $recipeId.'.eml can not find.';
            return False;
        }
        $resource = fopen($path, 'r');
        $parser = new MailMimeParser();
        $message = $parser->parse($resource);
        fclose($resource);

        $recipe['from'] = $message->getHeaderValue('From');
        $recipe['date'] = $message->getHeaderValue('Date');
        $recipe['recipeHtml']=$message->getHtmlContent();
        return $recipe;
    }
    public function getRecipeData($recipeId,$recipeConfigs){
        $recipe=$this->getRecipeHtml($recipeId);
        
        $date=$recipe['date'];
        $recipeHtml=$recipe['recipeHtml'];
        $email['recipe']=new Recipe();
        $email['recipe']['recieved_at']=$this->getDateTime($date);
        $email['recipe_header']=new RecipeHeader();
        foreach($recipeConfigs as $recipeConfig){
            foreach($recipeConfig as $key => $selector){
                if ( !strstr( $key, '_flag' ) or ($selector == NULL or $selector == '') ) 
                    continue;
                if( !strstr( $key, '_product_' ) && strstr( $key, 'recipe_' ) && ($selector !=NULL or $selector != '')){
                    $function=str_replace('_flag','',$key);
                    if($email['recipe_header'][$this->navigator[$function]] != '')
                        continue;
                    if(!method_exists($this,$function)) 
                        continue;
                    $result=$this->$function($recipeHtml,$selector);
                    if($result==FALSE)
                        continue;
                    $email['recipe_header'][$this->navigator[$function]]=$result;
                }
            }if($this->strip_tags==1){
                $email['recipe_data']=$this->recipe_product_list($this->getInnerText($recipeHtml,$recipeConfig->recipe_product_list_flag),$recipeConfig);    
            }else{
                if(array_key_exists('recipe_product_list_flag', $recipeConfig)){
                    $email['recipe_data']=$this->getInnerText($recipeHtml,$recipeConfig['recipe_product_list_flag']);
               }else{
                if(strstr( $key, '_product_' )){
                    $array=explode("---",$recipeConfig[array_keys($recipeConfig)[0]]);
                    $flag=array_keys($recipeConfig)[0];
                    $recipe_config=["recipe_product_list_flag"=>$array[0],$flag=>$array[1]];
                    $rows=$this->getInnerText($recipeHtml,$recipe_config['recipe_product_list_flag']);
                    $arr=[];
                    foreach($rows as $row){
                        $selector=$this->selectorList($recipe_config[$flag]);
                        // print_r($selectorList);die;
                        $result1=$this->getInnerText($row,$selector['selector'],$selector['position'],$selector['replace']);
                        if(is_array($result1)){
                            $result=implode( "----", $result1);
                        }else{
                            $result=$result1;
                        }
                        array_push($arr,$result);
                    }
                    $email['recipe_data']=$arr;
                    // $recipe=[$flag=>$array[1]];
                    // $obj = (object)$recipe;
                    // $email['recipe_data']=$this->recipe_product_list($this->getInnerText($recipeHtml,$array[0]),$obj);
                    // print_r($recipeConfig[array_keys($recipeConfig)[0]]);
                    // $selector=$this->selectorList($recipeConfig[array_keys($recipeConfig)[0]]);
                    // $email['recipe_data']=[
                    //     $flag=>$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace'])
                    // ];
                }
                }
            }
        }
        
        return $email;
    }




    
    private function getDateTime($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $time=date_create_from_format($dateFormat,$string);
            if($time){
                return date_format($time,$this->stdDateTimeFormat); 
                
            }
        }
    }
    private function selectorList($data){
        $selectors1=[];
        $selectors2=[];
        // echo $data;echo strstr( $data, '=>' );
        if(strstr( $data, '=>' )){
            $explodes=explode('=>',$data);
            foreach($explodes as $explode){
                array_push($selectors1,$explode);
            }
        }else{
            array_push($selectors1,$data);
        }
        foreach($selectors1 as $selec){
            $position=0;
            $replace='';
            $selector='';
            if(strstr($selec,'{')){
                $explodes=explode('{',$selec);
                $selector=trim($explodes[0]);
                $selec=trim(str_replace($selector,'',$selec));
                $explodes=explode('(',$selec);
                $position=trim(str_replace('{','',str_replace('}','',$explodes[0])));
                $replace=trim(str_replace(')','',$explodes[1]));
            }
            $sel=['selector'=>$selector,'position'=>$position,'replace'=>$replace];
            array_push($selectors2,$sel);
        }
        return $selectors2[0];
    }
    public function getInnerText($str,$elestr,$position='',$replace=''){
        $html = HtmlDomParser::str_get_html($str);
        $value='';
        $tables=array();$i=0;
        foreach($html->find($elestr) as $element) {
            if($this->strip_tags==1){
                $tables[$i]= trim($element->innertext);
            }else{
                $tables[$i]= $element->innertext;
            }
            $i++;
        }
        // print_r($elestr);
        if($position==NULL or $position==''){
            return $tables;
        }else{
            foreach ($tables as $key => $value) {
                if($this->strip_tags==1){
                    $tables[$key]=strip_tags($value);
                }else{
                    $tables[$key]=$value;
                }
                
            }
        }
        if($position>=0){
            if(count($tables)<abs($position)){
                // echo 'offset did not match';
                return False;
            }
            $value=$tables[$position-1];
            // return preg_replace('/'.$replace.'/','',$tables[$position-1]);
        }else{
            if(count($tables)<abs($position)){
                // echo 'offset did not match';
                return False;
            }
            $value=$tables[(count($tables)+$position)];
            // return preg_replace('/'.$replace.'/','',$tables[(count($tables)+$position)]);
        }
        // echo $replace.'--------------';
        if (!strstr($replace,' ')){
            $value=str_ireplace($replace,'',$value);
        }else{
            $replace=explode(' ',$replace);
            // print_r($replace);
            foreach ($replace as $val) {
                // echo '--pre '.$value.'->'.$val;
                if(substr_count($val, '/')==2){
                    $val=str_replace('/','',$val);
                    $value=preg_replace('/'.$val.'/','',$value);
                }
                $value=str_ireplace($val,'',$value);
                // echo '--post '.$value;
            }
        }
        return $value;
    }
    private function get_price($string){
        $string=str_ireplace('$','',$string);
        $string=preg_match('/(\d*.\d{2})/',$string,$match);
        if($string){
            if(is_numeric($match[0])){
                return $match[0];
            }
        }
        return False;
    }
    private function getDate($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $date=date_create_from_format($dateFormat,$string);
            if($date){
                return date_format($date,$this->stdDateFormat); 
                
            }
        }
    }
    private function getTime($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $time=date_create_from_format($dateFormat,$string);
            if($time){
                return date_format($time,$this->stdTimeFormat); 
                
            }
        }
    }









    //
    //Recipe Header Data Scrapping
    private function recipe_no($recipeHtml,$data){
        
        $recipe_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_no=trim($result);
            }else{
                $recipe_no=$result;
            }
        }
        return $recipe_no;
    }
    private function recipe_trans_no($recipeHtml,$data){
        
        $recipe_trans_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_trans_no=trim($result);
            }else{
                $recipe_trans_no=$result;
            }
        }
        return $recipe_trans_no;
    }
    private function recipe_trans_date($recipeHtml,$data){
        $recipe_trans_date='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_trans_date=$this->getDate(trim($result));
            }else{
                $recipe_trans_date=$result;
            }
        }
        return $recipe_trans_date;
    }
    private function recipe_trans_time($recipeHtml,$data){
        $recipe_trans_time='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_trans_time=$this->getTime(trim($result));
            }else{
                $recipe_trans_time=$result;
            }
        }
        return $recipe_trans_time;
    }
    private function recipe_terminal($recipeHtml,$data){
        
        $recipe_terminal='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_terminal=trim($result);
            }else{
                $recipe_terminal=$result;
            }
        }
        return $recipe_terminal;
    }
    private function recipe_employee_no($recipeHtml,$data){
        
        $recipe_employee_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_employee_no=trim($result);
            }else{
                $recipe_employee_no=$result;
            }
        }
        return $recipe_employee_no;
    }
    private function recipe_employee_name($recipeHtml,$data){
        
        $recipe_employee_name='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_employee_name=trim($result);
            }else{
                $recipe_employee_name=$result;
            }
        }
        return $recipe_employee_name;
    }
    private function recipe_subtotal_amount($recipeHtml,$data){
        $recipe_subtotal_amount='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_subtotal_amount=$this->get_price(trim($result));
            }else{
                $recipe_subtotal_amount=$result;
            }
            // $recipe_subtotal_amount=trim($result);
        }
        return $recipe_subtotal_amount;
    }
    private function recipe_total_tax($recipeHtml,$data){
        $recipe_total_tax='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_total_tax=$this->get_price(trim($result));
            }else{
                $recipe_total_tax=$result;
            }
        }
        return $recipe_total_tax;
    }
    private function recipe_total_tip($recipeHtml,$data){
        $recipe_total_tip='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_total_tip=$this->get_price(trim($result));
            }else{
                $recipe_total_tip=$result;
            }
        }
        return $recipe_total_tip;
    }
    private function recipe_delivery_fee($recipeHtml,$data){
        $recipe_delivery_fee='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_delivery_fee=$this->get_price(trim($result));
            }else{
                $recipe_delivery_fee=$result;
            }
        }
        return $recipe_delivery_fee;
    }
    private function recipe_total_amount($recipeHtml,$data){
        $recipe_total_amount='';
        $selector=$this->selectorList($data);
        // print_r($selector);
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_total_amount=$this->get_price(trim($result));
            }else{
                $recipe_total_amount=$result;
            }
        }
        return $recipe_total_amount;
    }
    private function recipe_note($recipeHtml,$data){
        
        $recipe_note='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_note=trim($result);
            }else{
                $recipe_note=$result;
            }
        }
        return $recipe_note;
    }
    //Recipe Product Data Scrapping
    private function recipe_product_list($recipeHtml,$recipeConfig){
        
        $recipe_product_list=False;
        // $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        // echo count($recipeHtml);die;
        // print_r($recipeHtml);die;
        
        if($this->strip_tags==1){
            foreach ($recipeHtml as $value) {
                $product_code='';
                $product_name='';
                $product_unit_price='';
                $product_quantity=1;
                $product_amount='';
                $product_code=$this->recipe_product_code($value,$recipeConfig->recipe_product_code_flag);
                $product_name=$this->recipe_product_name($value,$recipeConfig->recipe_product_name_flag);
                if(!$product_code && !$product_name){
                    continue;
                }
                // echo $value;
                $product_amount=$this->recipe_product_amount($value,$recipeConfig->recipe_product_amount_flag);
                if(!$product_amount){
                    continue;
                }
                $product_unit_price=$this->recipe_product_unit_price($value,$recipeConfig->recipe_product_unit_price_flag);
                $product_quantity=$this->recipe_product_quantity($value,$recipeConfig->recipe_product_quantity_flag);
                if($product_unit_price==''){
                    $product_unit_price=$product_amount;
                }
                $recipe_product_list[$product_name]=['product_id'=>$product_name,'price_per_unit'=>$product_unit_price,'quantity'=>$product_quantity,'amount'=>$product_amount];
            }
        }/*else{
            $data1=False;
            $data2=False;
            $i=0;
            foreach ($recipeHtml as $value) {
                foreach($recipeConfig as $key => $selector){
                    $function=str_replace("_flag","",$key);
                    $result=$this->$function($value,$selector);
                    if($result!=False){
                        $data1[str_replace("recipe_","",$function)]=$result;
                    }
                }
                $data2[$i++]=$data1;
            }
            print_r($data2);
            $recipe_product_list=$data2;
        }*/
        return $recipe_product_list;
    }
    private function recipe_product_code($recipeHtml,$data){
        
        $recipe_product_code='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_product_code=trim($result);
            }else{
                $recipe_product_code=$result;
            }
        }
        return $recipe_product_code;
    }
    private function recipe_product_name($recipeHtml,$data){
        $exceptKeyWords=['total','delivery','hst','tax','subtotal','tip'];
        $recipe_product_name='';
        $selector=$this->selectorList($data);
        // print_r($selector);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_product_name=trim($result);
            }else{
                $recipe_product_name=$result;
            }
        }
        $explodes=explode(' ',$recipe_product_name);
        foreach($exceptKeyWords as $exceptKeyWord){
            // echo $exceptKeyWord;
            // print_r($explodes);
            if(in_array($exceptKeyWord, array_map("strtolower", $explodes),true)){
                // echo $exceptKeyWord;
                $recipe_product_name='';
            }
        }
        return $recipe_product_name;
    }
    private function recipe_product_unit_price($recipeHtml,$data){
        
        $recipe_product_unit_price='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_product_unit_price=trim($result);
            }else{
                $recipe_product_unit_price=$result;
            }
            // $recipe_product_unit_price=trim($result);
        }
        return $recipe_product_unit_price;
    }
    private function recipe_product_quantity($recipeHtml,$data){
        
        $recipe_product_quantity=1;
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_product_quantity=trim($result);
            }else{
                $recipe_product_quantity=$result;
            }
        }
        return $recipe_product_quantity;
    }
    private function recipe_product_amount($recipeHtml,$data){
        
        $recipe_product_amount='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            if($this->strip_tags==1){
                $recipe_product_amount=$this->get_price(trim($result));
            }else{
                $recipe_product_amount=$result;
            }
            // $recipe_product_amount=trim($result);
        }
        return $recipe_product_amount;
    }
}
