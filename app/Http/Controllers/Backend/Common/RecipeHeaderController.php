<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\RecipeHeader;
use Illuminate\Http\Request;

class RecipeHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Common\RecipeHeader  $recipeHeader
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeHeader $recipeHeader)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Common\RecipeHeader  $recipeHeader
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeHeader $recipeHeader)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Common\RecipeHeader  $recipeHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeHeader $recipeHeader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Common\RecipeHeader  $recipeHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeHeader $recipeHeader)
    {
        //
    }
}
