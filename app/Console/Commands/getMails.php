<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Webklex\IMAP\Client;
use App\Models\Common\Recipe;
use App\Http\Controllers\Backend\Common\RecipeController;

class getMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:reciept';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Reciepts From email account and save it to storage/emails as autoIncrement id.eml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->oClient = new Client([
            'host'          => 'imap.zoho.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => 'minesh.sulakshana@zohomail.com',
            'password'      => 'secret#123',
            'protocol'      => 'imap',
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        
        $this->oClient->connect();
        $aFolder = $this->oClient->getFolder('inbox');
        $aMessage = $aFolder->messages()->all()->get();
        foreach($aMessage as $message){
            if(isset($message->bodies['html'])){
                $reciept=new Recipe();
                $reciept->mail_from=$message->getFrom()[0]->mail;
                $reciept->downloaded_at=DATE('Y-m-d H:i:s');
                if($reciept->save()){
                    $reciept_id = $reciept->id;
                }
                $folderPath=storage_path('emails');
                $myfile = fopen($folderPath.'\\'.$reciept_id.".eml", "w");
                fwrite($myfile, $message->header);
                if(isset($message->bodies['text'])){
                    fwrite($myfile, '
--'.$message->getStructure()->parameters[0]->value);
                    fwrite($myfile,'
Content-Type: text/plain; charset="UTF-8"\n
Content-Transfer-Encoding: quoted-printable

');
                    fwrite($myfile, $message->bodies['text']->content);
                }
                fwrite($myfile, '
--'.$message->getStructure()->parameters[0]->value);
                fwrite($myfile,'
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

');
                fwrite($myfile, $message->bodies['html']->content);

                $message->moveToFolder('INBOX.read');
                fclose($myfile);
            }
        }
    }
}
