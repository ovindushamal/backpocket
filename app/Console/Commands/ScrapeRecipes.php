<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Common\Recipe;
use App\Models\Common\RecipeHeader;
use App\Models\Common\RecipeData;
use App\Models\Common\Product;
use Illuminate\Support\Facades\DB;
use ZBateson\MailMimeParser\MailMimeParser;
use ZBateson\MailMimeParser\Message;
use KubAT\PhpSimple\HtmlDomParser;
// use Symfony\Component\DomCrawler\Crawler;

class ScrapeRecipes extends Command
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:reciepts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'scrape downloaded reciepts and store recipe data on recipe_headers,recipe_data tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->recipeHeader=new RecipeHeader();
        $this->recipeData=new RecipeData();
        $this->explodeChar=[' ',',','$',' × '];//
        $this->stdDateFormat='Y-m-d';
        $this->stdTimeFormat='H:i:s';
        $this->stdDateTimeFormat='Y-m-d H:i:s';
        $this->dateFormats=['H:i:s','d/m/Y, H:i','D, d M Y H:i:s O','M d, Y'];
        $this->navigator=['recipe_no'=>'recipe_no','recipe_trans_no'=>'trans_no','recipe_trans_date'=>'trans_date','recipe_trans_time'=>'trans_time','recipe_terminal'=>'terminal','recipe_employee_no'=>'employee_no','recipe_employee_name'=>'employee_name','recipe_subtotal_amount'=>'subtotal_amount','recipe_total_tax'=>'total_tax','recipe_total_tip'=>'total_tip','recipe_delivery_fee'=>'delivery_fee','recipe_total_amount'=>'total_amount','recipe_note'=>'note'];
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $email = False ;
        $recipeIds=$this->getRecipeIds();
        if ($recipeIds==False){
            echo 'No Recipes to scrape.';return False;
        }
        foreach ($recipeIds as $recipeId){
            $recipeId=$recipeId->id;
            $recipe=$this->scrapeRecipe($recipeId);
            if(!$recipe){
                continue;
            }
            $email[$recipeId]=$recipe;
            
        }
        if ($email == False)
            return False;

        foreach($email as$reciept_no => $reciept){
            $recipe=new Recipe();
            $recipeHeader=new RecipeHeader();
            
            $recipe->id=$reciept_no;
            foreach($reciept['recipe'] as $key => $value){
                $recipe->$key = $value;
            }
            $recipe->scrape_at=DATE($this->stdDateTimeFormat);
            //DB Entry $recipe
            $recipeHeader->recipe_id=$reciept_no;
            foreach($reciept['recipe_header'] as $key => $value){
                $recipeHeader->$key = $value;
            }
            //DB Entry $recipeHeader
            if(!$reciept['recipe_data']){
                continue;
            }
            foreach($reciept['recipe_data'] as $product){
                $recipeData=new RecipeData();
                $recipeData->recipe_id=$reciept_no;
                foreach($product as $key => $value){
                    $recipeData->$key = $value;
                }
                //DB Entry $recipeData
            }
            
        }
        echo json_encode($email);

    }
    // this getRecipeHtml coppied from RecipeController@getRecipeHtml
    public function getRecipeHtml($recipeId){
        $folderPath=storage_path('emails');
        $path=$folderPath.'\\'.$recipeId.'.eml';

        if(!file_exists($path)) {
            echo $recipeId.'.eml can not find.';
            return False;
        }
        $resource = fopen($path, 'r');
        $parser = new MailMimeParser();
        $message = $parser->parse($resource);
        fclose($resource);

        $recipe['from'] = $message->getHeaderValue('From');
        $recipe['date'] = $message->getHeaderValue('Date');
        $recipe['recipeHtml']=$message->getHtmlContent();
        return $recipe;
    }
    /*public function getRecipeHtml($recipeId){
        $path=$this->folderPath.'\\'.$recipeId.'.eml';

        if(!file_exists($path)) {
            echo $recipeId.'.eml can not find.';
            return False;
        }
        $resource = fopen($path, 'r');
        $message = $this->parser->parse($resource);
        fclose($resource);

        $recipe['from'] = $message->getHeaderValue('From');
        $recipe['date'] = $message->getHeaderValue('Date');
        $recipe['recipeHtml']=$message->getHtmlContent();
        return $recipe;
    }*/
    public function scrapeRecipe($recipeId){
            $email=False;
            $recipe=$this->getRecipeHtml($recipeId);
            if(!$recipe){return False;}
            $from=$recipe['from'];
            // if($recipeId==3){
            //     echo $recipeHtml;die; // only for testing
            // }
            $vendors=$this->getVendorId($from);
            if ($vendors==False){
                echo 'Vendor can not find.';
                return False;
            }
            $vendorId=$vendors[0]->id;
            $recipeConfigs=$this->getRecipeConfigs($vendorId);
            if($recipeConfigs==False){
                return False;
            }
            // print_r($recipeConfigs); // only for testing
            $email=$this->getRecipeData($recipe,$recipeConfigs);
            $email['recipe_header']['recipe_id']=$recipeId;
            $email['recipe_header']['vendor_id']=$vendorId;
            return $email;
    }
    public function getRecipeData($recipe,$recipeConfigs){
        
        $date=$recipe['date'];
        $recipeHtml=$recipe['recipeHtml'];
        $email['recipe']=new Recipe();
        $email['recipe']['recieved_at']=$this->getDateTime($date);
        $email['recipe_header']=new RecipeHeader();
        foreach($recipeConfigs as $recipeConfig){
            foreach($recipeConfig as $key => $selector){
                if ( !strstr( $key, '_flag' ) or ($selector == NULL or $selector == '') ) 
                    continue;
                if( !strstr( $key, '_product_' ) && strstr( $key, 'recipe_' ) && ($selector !=NULL or $selector != '')){
                    $function=str_replace('_flag','',$key);
                    if($email['recipe_header'][$this->navigator[$function]] != '')
                        continue;
                    if(!method_exists($this,$function)) 
                        continue;
                    $result=$this->$function($recipeHtml,$selector);
                    if($result==FALSE)
                        continue;
                    $email['recipe_header'][$this->navigator[$function]]=$result;
                }
            }
            $email['recipe_data']=$this->recipe_product_list($this->getInnerText($recipeHtml,$recipeConfig->recipe_product_list_flag),$recipeConfig);
                
        }
        
        return $email;
    }
    private function getRecipeIds(){

        $recipes = DB::table('recipes')->where('scrape_at',NULL)->get('id');
        if (count($recipes)!=0)
            return $recipes;
        else    
            return False;

    }
    private function getVendorId($email){

        $vendors = DB::table('vendors')->where('email',$email)->get('id');
        if (count($vendors)!=0)
            return $vendors;
        else    
            return False;

    }
    private function getRecipeConfigs($vendorId){

        $recipeConfig = DB::table('recipe_configs')->where('vendor_id',$vendorId)->get();
        if (count($recipeConfig)!=0)
            return $recipeConfig;
        else    
            return False;
    }
    private function selectorList($data){
        $selectors1=[];
        $selectors2=[];
        // echo $data;echo strstr( $data, '=>' );
        if(strstr( $data, '=>' )){
            $explodes=explode('=>',$data);
            foreach($explodes as $explode){
                array_push($selectors1,$explode);
            }
        }else{
            array_push($selectors1,$data);
        }
        foreach($selectors1 as $selec){
            $position=0;
            $replace='';
            $selector='';
            if(strstr($selec,'{')){
                $explodes=explode('{',$selec);
                $selector=trim($explodes[0]);
                $selec=trim(str_replace($selector,'',$selec));
                $explodes=explode('(',$selec);
                $position=trim(str_replace('{','',str_replace('}','',$explodes[0])));
                $replace=trim(str_replace(')','',$explodes[1]));
            }
            $sel=['selector'=>$selector,'position'=>$position,'replace'=>$replace];
            array_push($selectors2,$sel);
        }
        return $selectors2[0];
    }
    // DB entry Want to check
    private function getProductId($product){
        $product_id = False;
        $products = DB::table('products')->where('name',$product)->orderBy('created_at', 'asc')->limit(1)->get();
        if (count($products)!=0){
            $product_id = $products->id;
        }else {
            $product=new Product();
            $product->name=$product;
            $product->active=True;
            if($product->save()){
                $product_id = $product->id;
            }
        }
        return $product_id;
    }
    public function getInnerText($str,$elestr,$position='',$replace=''){
        $html = HtmlDomParser::str_get_html($str);
        $value='';
        $tables=array();$i=0;
        foreach($html->find($elestr) as $element) {
            $tables[$i]= trim($element->innertext);
            $i++;
        }
        if($position==NULL or $position==''){
            return $tables;
        }else{
            foreach ($tables as $key => $value) {
                $tables[$key]=strip_tags($value);
            }
        }
        if($position>=0){
            if(count($tables)<abs($position)){
                // echo 'offset did not match';
                return False;
            }
            $value=$tables[$position-1];
            // return preg_replace('/'.$replace.'/','',$tables[$position-1]);
        }else{
            if(count($tables)<abs($position)){
                // echo 'offset did not match';
                return False;
            }
            $value=$tables[(count($tables)+$position)];
            // return preg_replace('/'.$replace.'/','',$tables[(count($tables)+$position)]);
        }
        // echo $replace.'--------------';
        if (!strstr($replace,' ')){
            $value=str_ireplace($replace,'',$value);
        }else{
            $replace=explode(' ',$replace);
            // print_r($replace);
            foreach ($replace as $val) {
                // echo '--pre '.$value.'->'.$val;
                if(substr_count($val, '/')==2){
                    $val=str_replace('/','',$val);
                    $value=preg_replace('/'.$val.'/','',$value);
                }
                $value=str_ireplace($val,'',$value);
                // echo '--post '.$value;
            }
        }
        return $value;
    }
    private function get_price($string){
        $string=str_ireplace('$','',$string);
        $string=preg_match('/(\d*.\d{2})/',$string,$match);
        if($string){
            if(is_numeric($match[0])){
                return $match[0];
            }
        }
        return False;
    }
    private function getDate($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $date=date_create_from_format($dateFormat,$string);
            if($date){
                return date_format($date,$this->stdDateFormat); 
                
            }
        }
    }
    private function getTime($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $time=date_create_from_format($dateFormat,$string);
            if($time){
                return date_format($time,$this->stdTimeFormat); 
                
            }
        }
    }
    private function getDateTime($string){
        // echo $string;die;
        foreach($this->dateFormats as $dateFormat){
            $time=date_create_from_format($dateFormat,$string);
            if($time){
                return date_format($time,$this->stdDateTimeFormat); 
                
            }
        }
    }
    
    //Recipe Header Data Scrapping
    private function recipe_no($recipeHtml,$data){
        
        $recipe_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_no=trim($result);
        }
        return $recipe_no;
    }
    private function recipe_trans_no($recipeHtml,$data){
        
        $recipe_trans_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_trans_no=trim($result);
        }
        return $recipe_trans_no;
    }
    private function recipe_trans_date($recipeHtml,$data){
        $recipe_trans_date='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_trans_date=$this->getDate(trim($result));
        }
        return $recipe_trans_date;
    }
    private function recipe_trans_time($recipeHtml,$data){
        $recipe_trans_time='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_trans_time=$this->getTime(trim($result));
        }
        return $recipe_trans_time;
    }
    private function recipe_terminal($recipeHtml,$data){
        
        $recipe_terminal='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_terminal=trim($result);
        }
        return $recipe_terminal;
    }
    private function recipe_employee_no($recipeHtml,$data){
        
        $recipe_employee_no='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_employee_no=trim($result);
        }
        return $recipe_employee_no;
    }
    private function recipe_employee_name($recipeHtml,$data){
        
        $recipe_employee_name='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_employee_name=trim($result);
        }
        return $recipe_employee_name;
    }
    private function recipe_subtotal_amount($recipeHtml,$data){
        $recipe_subtotal_amount='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            // $recipe_subtotal_amount=trim($result);
            $recipe_subtotal_amount=$this->get_price(trim($result));
        }
        return $recipe_subtotal_amount;
    }
    private function recipe_total_tax($recipeHtml,$data){
        $recipe_total_tax='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_total_tax=$this->get_price(trim($result));
        }
        return $recipe_total_tax;
    }
    private function recipe_total_tip($recipeHtml,$data){
        $recipe_total_tip='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_total_tip=$this->get_price(trim($result));
        }
        return $recipe_total_tip;
    }
    private function recipe_delivery_fee($recipeHtml,$data){
        $recipe_delivery_fee='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_delivery_fee=$this->get_price(trim($result));
        }
        return $recipe_delivery_fee;
    }
    private function recipe_total_amount($recipeHtml,$data){
        $recipe_total_amount='';
        $selector=$this->selectorList($data);
        // print_r($selector);
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            // $recipe_total_amount=trim($result);
            $recipe_total_amount=$this->get_price(trim($result));
        }
        return $recipe_total_amount;
    }
    private function recipe_note($recipeHtml,$data){
        
        $recipe_note='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_note=trim($result);
        }
        return $recipe_note;
    }
    //Recipe Product Data Scrapping
    private function recipe_product_list($recipeHtml,$recipeConfig){
        
        $recipe_product_list=False;
        // $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        // echo count($recipeHtml);die;
        // print_r($recipeHtml);die;
        foreach ($recipeHtml as $value) {
            $product_code='';
            $product_name='';
            $product_unit_price='';
            $product_quantity=1;
            $product_amount='';
            $product_code=$this->recipe_product_code($value,$recipeConfig->recipe_product_code_flag);
            $product_name=$this->recipe_product_name($value,$recipeConfig->recipe_product_name_flag);
            if(!$product_code && !$product_name){
                continue;
            }
            // echo $value;
            $product_amount=$this->recipe_product_amount($value,$recipeConfig->recipe_product_amount_flag);
            if(!$product_amount){
                continue;
            }
            $product_unit_price=$this->recipe_product_unit_price($value,$recipeConfig->recipe_product_unit_price_flag);
            $product_quantity=$this->recipe_product_quantity($value,$recipeConfig->recipe_product_quantity_flag);
            if($product_unit_price==''){
                $product_unit_price=$product_amount;
            }//uncomment after DB Entry in $this->getProductId()
            // if($product_name == ''){
            //     $product_id=$this->getProductId($product_code);
            // }else{
            //     $product_id=$this->getProductId($product_name);
            // }
            $product_id=$product_name;//for temporary
            $recipe_product_list[$product_name]=['product_id'=>$product_id,'price_per_unit'=>$product_unit_price,'quantity'=>$product_quantity,'amount'=>$product_amount];
        }
        return $recipe_product_list;
    }
    private function recipe_product_code($recipeHtml,$data){
        
        $recipe_product_code='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_product_code=trim($result);
        }
        return $recipe_product_code;
    }
    private function recipe_product_name($recipeHtml,$data){
        $exceptKeyWords=['total','delivery','hst','tax','subtotal','tip'];
        $recipe_product_name='';
        $selector=$this->selectorList($data);
        // print_r($selector);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_product_name=trim($result);
        }
        $explodes=explode(' ',$recipe_product_name);
        foreach($exceptKeyWords as $exceptKeyWord){
            // echo $exceptKeyWord;
            // print_r($explodes);
            if(in_array($exceptKeyWord, array_map("strtolower", $explodes),true)){
                // echo $exceptKeyWord;
                $recipe_product_name='';
            }
        }
        return $recipe_product_name;
    }
    private function recipe_product_unit_price($recipeHtml,$data){
        
        $recipe_product_unit_price='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            // $recipe_product_unit_price=trim($result);
            $recipe_product_unit_price=trim($result);
        }
        return $recipe_product_unit_price;
    }
    private function recipe_product_quantity($recipeHtml,$data){
        
        $recipe_product_quantity=1;
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            $recipe_product_quantity=trim($result);
        }
        return $recipe_product_quantity;
    }
    private function recipe_product_amount($recipeHtml,$data){
        
        $recipe_product_amount='';
        $selector=$this->selectorList($data);
        // print_r($selectorList);die;
        $result=$this->getInnerText($recipeHtml,$selector['selector'],$selector['position'],$selector['replace']);
        if($result!=False){
            // $recipe_product_amount=trim($result);
            $recipe_product_amount=$this->get_price(trim($result));
        }
        return $recipe_product_amount;
    }
    //Vendor Data Scrapping
    private function vendor_name($dataSet){
        $vendor_name='';
        foreach ($dataSet as $data) {
            if ($data == null or trim($data) == '')
                continue;
            if (strstr($vendor_name,trim($data)))
                continue;
            $data=trim($data);
            $vendor_name.=$data.' ';
        }
        return trim($vendor_name);

    }
    private function vendor_address($dataSet){

        $vendor_address='';
        foreach ($dataSet as $data) {
            if ($data == null or trim($data) == '')
                continue;
            if (strstr($vendor_address,trim($data)))
                continue;
            $data=trim($data);
            $vendor_address.=$data.' ';
        }
        return trim($vendor_address);

    }
    private function vendor_email($dataSet){

        $vendor_email='';
        foreach ($dataSet as $data) {
            if ($data == null or trim($data) =='')
                continue;
            $data=trim($data);
            if (strstr($data,'@'))
                $vendor_email=$data;
        }
        return trim($vendor_email);
        
    }
    private function vendor_contact_no($dataSet){

        $vendor_contact_no='';
        foreach($dataSet as $data){
            if ($data == null or trim($data) =='')
                continue;

        }
        return $vendor_contact_no;
    }
    private function vendor_store($dataSet){
        
        $vendor_store='';
        foreach($dataSet as $data){
            if ($data == null or trim($data) =='')
                continue;

        }
        return $vendor_store;
    }
}