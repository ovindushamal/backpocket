<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = ['mail_from', 'recieved_at', 'downloaded_at', 'scrape_at'];
}
