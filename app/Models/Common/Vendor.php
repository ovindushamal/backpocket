<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $fillable = ['name', 'email', 'store', 'address', 'tax','contact_no','active'];
}
