<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = ['name', 'active'];
}