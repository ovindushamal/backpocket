<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\Common\VendorController;
use App\Http\Controllers\Backend\Common\RecipeController;
use App\Http\Controllers\Backend\Common\RecipeConfigController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('vendor', [VendorController::class, 'index'])->name('vendor');
Route::get('receipt', [RecipeController::class, 'index'])->name('receipt');
Route::get('ajax/ajaxVendor',[VendorController::class,'ajaxVendor']);
Route::post('ajax/ajaxVendorPost',[VendorController::class,'ajaxVendor']);
Route::get('ajax/ajaxReceipt',[RecipeController::class,'ajaxReceipt']);
Route::get('ajax/ajaxReceiptConfig',[RecipeConfigController::class,'ajaxReceiptConfig']);
