<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});
Breadcrumbs::for('admin.vendor', function ($trail) {
    $trail->push('Vendor Management', route('admin.vendor'));
});
Breadcrumbs::for('admin.receipt', function ($trail) {
    $trail->push('Receipt Management', route('admin.receipt'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
